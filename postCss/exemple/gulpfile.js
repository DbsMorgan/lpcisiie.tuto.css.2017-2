var gulp = require("gulp");
var postcss = require("gulp-postcss");
var sass = require("gulp-sass");

var cssnano = require("cssnano")

gulp.task("css", function() {
	return gulp.src("./src/*.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(gulp.dest("./dest"))
});

gulp.task("cssnano", function() {
	var processors = [
		cssnano
	];
	return gulp.src("./src/*.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(postcss(processors))
		.pipe(gulp.dest("./dest"))
});