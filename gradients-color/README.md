# Tutoriel LP CISIIE

## module html avancé

## thème du tutoriel : CSS Gradients et CSS Color modul

## auteurs : 
Myriam MATMAT 
Nicolas JACQUEMIN
Lucas Marquant 
Mohamed ALHASNE

-> Contient le tutoriel au format .docx et .pdf, 
	I) Introduction, rappels et pointeur vers la spécification W3C et une référence de qualité complète 
	II) Explication des dégradations des couleurs en CSS, et du color module css
		1.	Les avantages pour CSS Gradients
		2.	Les différents types de dégradations pour CSS Gradients
		3.	Explication du color module
	III) La démo elle-même, avec les sources
	IV) Un point sur l'implantation dans les navigateurs
	V) Une liste de ressources/exemples/tutoriels existant sur le même thème
	
-> Le diaporama utilisé lors de la présentation orale
	Présentant et expliquant Css gradients et Css Color, leur implantation sur les navigateurs et introduit une démonstration dans les sources sont jointes dans le dossier src

-> Le dossier src contenant les sources utilisées pour illustrer le tutoriel et la présentation, avec : 
	Un dossier contenant le fichier Html, Css et l'image utilisés pour Linear-gradient() et repeating-linear-gradient,
	Un dossier contenant le fichier Html et Css utilisés pour radial-gradient() et repeating-radial-gradient,
	Un dossier contenant le Hmtl, le Css, deux images dont une indiquant la liste des propriétés où il est possible d'utiliser Css Color.