# Tutoriel LP CISIIE

## module html avancé

## thème  du tutoriel : CSS ANIMATION

## auteurs :
BISELX Charles
DEMARBRE Allan
ESCAMILLA Valentin
PENGUILLY Bertrand

##Installation
Placez le dossier css à la base du dossier public de votre projet et indiquez le nom du fichier css à lier comme par exemple :
<link rel="stylesheet" href="css/nom_fichier.css">

##Fonctionnement
dans une balise dans le fichier css, les fonctions ci-dessous permettent d'appeler une keyframe qui va faire fonctionner l'animation et donner la durée de l'animation.
-webkit-animation-name: exampleun; /* Safari 4.0 - 8.0 */
-webkit-animation-duration: 4s; /* Safari 4.0 - 8.0 */
animation-name: exampleun;
animation-duration: 4s;


les keyframes pertettant de regouper des paramètres comme par exemple de changer la couleur de fond du rouge au jaune.

    /* Safari 4.0 - 8.0 */
    @-webkit-keyframes exampleun {
        from {background-color: red;}
        to {background-color: yellow;}
    }

    /* Standard syntax */
    @keyframes exampleun {
        from {background-color: red;}
        to {background-color: yellow;}
    }

les -webkit- devant les paramètres est une librairie permettant de réaliser les mêmes fonctionnalités sous certains navigateurs

d'autres option utiles :

permet de décaller le début de l'animation
animation-delay: 1s;

le nombre d'itération répété pour l'animation
animation-iteration-count: 3

répéter indéfiniment
animation: infinite ;

refait l'animation en sens inverse
animation: alternate ;

différent type de variation
animation-timing-function: linear;      = linéaire
animation-timing-function: ease;        = atténuer (rapide au debut lent à la fin)
animation-timing-function: ease-in;     = atténuer (lent au debut rapide à la fin)
animation-timing-function: ease-out;    = atténuer comme ease mais plus adouci à la fin
animation-timing-function: ease-in-out; = atténuer au début et à la fin
